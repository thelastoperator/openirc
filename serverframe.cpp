#include "serverframe.h"
#include "ui_serverframe.h"

ServerFrame::ServerFrame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ServerFrame)
{
    ui->setupUi(this);
}

ServerFrame::~ServerFrame()
{
    delete ui;
}
