#include "channelframe.h"
#include "ui_channelframe.h"

ChannelFrame::ChannelFrame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ChannelFrame)
{
    ui->setupUi(this);
}

ChannelFrame::~ChannelFrame()
{
    delete ui;
}
