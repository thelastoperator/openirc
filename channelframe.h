#ifndef CHANNELFRAME_H
#define CHANNELFRAME_H

#include <QFrame>

namespace Ui {
class ChannelFrame;
}

class ChannelFrame : public QFrame
{
    Q_OBJECT

public:
    explicit ChannelFrame(QWidget *parent = 0);
    ~ChannelFrame();

private:
    Ui::ChannelFrame *ui;
};

#endif // CHANNELFRAME_H
